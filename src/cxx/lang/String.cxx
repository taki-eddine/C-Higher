////////////////////////////////////////////////////////////////////////////////
#include "cxx/lang/String.hpp"
/******************************************************************************/
#include <cstring>
#include <algorithm>
#include "cxx/lang/numeric/NumberTraits.hpp"
#include "cxx/utils/Memory.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace        cxx  {
inline namespace lang {

// === [Private] ===============================================================
//--------------------------------------
void
String::_makeGap (u32 offset, u32 count)
{  reserve(_size + count);
   _cstr[_size + count] = '\0';  // close the string.

   if ( _size ) // create a gap only if we have a data.
      for (u32 i = _size + count, j = _size; (j >= offset) && i; --i, --j)
         _cstr[i] = _cstr[j];

   // - only in debug to visualize the insert space - //
   for (u32 i = offset, end = offset + count; i < end; ++i)
      _cstr[i] = '~';
}

// === [Public] ================================================================
//--------------------------------------
//! @todo: must init capacity with 1, and "_cstr" take empty string.
String::String (void)
: _size(0)
, _capacity(1)
, _cstr(mAlloc(1))
{ }

//--------------------------------------
String::String (const String &cp)
: _size(cp._size)
, _capacity(cp._capacity)
, _cstr(mAlloc(cp._capacity))
{ String::copy(_cstr, cp._cstr); }

//--------------------------------------
String::String (String &&mv)
: _size(mv._size)
, _capacity(mv._capacity)
, _cstr(mv._cstr)
{  mv._cstr = nullptr;
   mv._capacity = mv._size = 0;
}

//--------------------------------------
String::String (const char *cstr)
: _size(std::strlen(cstr))
, _capacity(_size + 1)
, _cstr(mAlloc(_capacity))
{ String::copy(_cstr, cstr); }

//--------------------------------------
String::String (const char *cstr, std::size_t len)
: _size(len)
, _capacity(_size + 1)
, _cstr(mAlloc(_capacity))
{ this->mCopy(_cstr, cstr, _size); }

//--------------------------------------
String::String (const bool value)
: _size(std::strlen("false\0true" + (6 * value)))
, _capacity(_size + 1)
, _cstr(mAlloc(_capacity))
{ this->mCopy(_cstr, "false\0true" + (6 * value), _size); }

//--------------------------------------
String::~String (void)
{ this->mDealloc(_cstr); }

//--------------------------------------
String::operator const char * () const
{ return _cstr; }

//--------------------------------------
String &
String::operator = (const char* cstr)
{  clear();
   insert(0, cstr);
   return *this;
}

//--------------------------------------
/*const String&
String::operator = (String &&str)
{ }*/

//--------------------------------------
String&
String::operator += (String const &str)
{  append(str);
   return *this;
}

//--------------------------------------
String&
String::operator += (char const c)
{  append(c);
   return *this;
}

//--------------------------------------
String&
String::operator += (char const *cstr)
{  append(cstr);
   return *this;
}

//--------------------------------------
void
String::shrink (void)
{  if ( _capacity > (_size+1) )
   {  _capacity = _size + 1;
      _cstr = this->mAllocAndMove(_capacity, _cstr, _size);
   }
}

//--------------------------------------
void
String::assign (char c, u32 count)
{  clear();
   for ( ; count; --count)
      append(c);
}

//--------------------------------------
void
String::assign (const char *cstr, u32 n)
{  clear();
   reserve( (n = std::min((std::size_t)n, std::strlen(cstr))) );

   for (u32 i = 0; i < n; ++i)
      append(cstr[i]);
}

//--------------------------------------
void
String::assign (const char *cstr)
{  assign(cstr, std::strlen(cstr));  }

//--------------------------------------
void
String::assign (const String &str, u32 n)
{  assign(str._cstr, n);  }

//--------------------------------------
void
String::assign (String &&str)
{  str._cstr     = str._cstr;
   str._size     = str._size;
   str._capacity = str._capacity;

   str._cstr     = nullptr;
   str._size     = 0;
   str._capacity = 0;
}

//--------------------------------------
bool
String::equals (const String &str)
{ return String::equals(_cstr, str._cstr); }

//--------------------------------------
bool
String::equals (const char *cstr)
{ return String::equals(_cstr, cstr); }

//--------------------------------------
int
String::insert (u32 offset, const String &str)
{  insert(offset, str._cstr);
   return str._size;
}

//--------------------------------------
int
String::insert (u32 offset, const char *cstr)
{  u32 str_len = std::strlen(cstr);

   _makeGap(offset, str_len);
   mCopy(_cstr + offset, cstr, str_len);
   _size += str_len;
   return str_len;
}

//--------------------------------------
int
String::insert (u32 offset, const char c)
{  _makeGap(offset, 1);
   _cstr[offset] = c;
   ++_size;
   return 1;
}

//--------------------------------------
const char *
String::cStr () const
{ return _cstr; }

//--------------------------------------
const String &
String::toString () const
{ return *this; }


//------------------------------------------------------------------------------
template int String::insert (u32 offset, int num);
template int String::insert (u32 offset, long num);
template int String::insert (u32 offset, long long num);

template int String::insert (u32 offset, unsigned long num);
template int String::insert (u32 offset, unsigned long long num);

template int String::insert (u32 offset, float num);
template int String::insert (u32 offset, double num);
template int String::insert (u32 offset, long double num);
//----------------------------------------------------------------------------//

}} // namespace cxx::lang
