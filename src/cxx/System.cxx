////////////////////////////////////////////////////////////////////////////////
#include "cxx/System.hpp"
/******************************************************************************/
#include <stdlib.h>
#include <unistd.h>
#include "cxx/lang/Defines.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx)

//! @todo: Make it inline
void
cmd(char* const cmd[]) {
    (void)cmd;
#ifdef _linux_
    if( !fork() ) {
        execvp(*cmd, cmd);
        exit(127);
    }
#endif
}

_end_namespace_(cxx)
