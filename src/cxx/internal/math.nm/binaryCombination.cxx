////////////////////////////////////////////////////////////////////////////////
#include "cxx/math.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace        cxx  {
inline namespace math {

// Private data ------------------------
namespace priv{

    template<> u32 binaryCombinationHelper(const u8* in,  std::vector<u8>& out, int start, int n, int index, int insert);
    template<> u32 binaryCombinationHelper(const u16* in, std::vector<u16>& out, int start, int n, int index, int insert);
    template<> u32 binaryCombinationHelper(const u32* in, std::vector<u32>& out, int start, int n, int index, int insert);
    template<> u32 binaryCombinationHelper(const u64* in, std::vector<u64>& out, int start, int n, int index, int insert);

} // namespace priv
//----------------------------------------------------------------------------//

// Template explicit instantiation -----
template<> std::vector<u8>  binaryCombination(std::initializer_list<u8>  list);
template<> std::vector<u16> binaryCombination(std::initializer_list<u16> list);
template<> std::vector<u32> binaryCombination(std::initializer_list<u32> list);
template<> std::vector<u64> binaryCombination(std::initializer_list<u64> list);
//----------------------------------------------------------------------------//

} // inline namespace math
} // namespace cxx
