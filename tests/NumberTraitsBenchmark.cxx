////////////////////////////////////////////////////////////////////////////////
#include <cmath>
#include <celero/Celero.h>
CELERO_MAIN
using celero::TestFixture;
#include "cxx/lang/numeric/NumberTraits.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace        cxx   {
inline namespace utils {

/** @brief UtilsBenchmark class. **/
class NumberTraitsBenchmark : public TestFixture
{   protected:
      unsigned u_value  = 123456789u;
      int      sp_value = 123456789;
      int      sn_value = -123456789;
      float    fp_value = 123.45678f;
      float    fn_value = -123.45678f;
      double   dp_value = 123.45678;
      double   dn_value = -123.45678;
};

//--------------------------------------
BASELINE_F(CountDigits, std_unsigned, NumberTraitsBenchmark, 100, 10'000)
{ (u_value == 0 ? 1 : int(std::log10(std::abs(u_value)) + 1)); }

BENCHMARK_F(CountDigits, cxx_unsigned, NumberTraitsBenchmark, 100, 10'000)
{ countDigits(u_value); }

//--------------------------------------
BASELINE_F(CountDigits, std_signed_pos, NumberTraitsBenchmark, 100, 10'000)
{ (sp_value == 0 ? 1 : int(std::log10(std::abs(sp_value)) + 1)); }

BENCHMARK_F(CountDigits, cxx_signed_pos, NumberTraitsBenchmark, 100, 10'000)
{ countDigits(sp_value); }

//--------------------------------------
BASELINE_F(CountDigits, std_signed_neg, NumberTraitsBenchmark, 100, 10'000)
{ (sn_value == 0 ? 1 : int(std::log10(std::abs(sn_value)) + 1)); }

BENCHMARK_F(CountDigits, cxx_signed_neg, NumberTraitsBenchmark, 100, 10'000)
{ countDigits(sn_value); }

}} // namespace cxx::utils