////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdlib.h>
#include <cxxabi.h>
////////////////////////////////////////////////////////////////////////////////

//--------------------------------------
#define EXPECT_STREAMOUT_EQ(expect, stream_call) \
   do \
   {  int  stdout_save      = dup(STDOUT_FILENO); \
      char stream_buf[2024] = ""; \
\
      /* - save and set the printf buffer - */ \
      std::freopen("/dev/null", "a", stdout); \
      std::fflush(stdout); \
      std::setbuf(stdout, stream_buf); \
\
      /* - print - */ \
      stream_call; \
\
      /* - restore the stdout - */ \
      std::freopen("/dev/null", "a", stdout); \
      dup2(stdout_save, STDOUT_FILENO); \
      std::fflush(stdout); \
      std::setbuf(stdout, nullptr); \
\
      EXPECT_STREQ(expect, stream_buf); \
   } while (0)

//--------------------------------------
#define EXPECT_TYPE_EQ(expected, tp) \
  do \
  {   int status = 0; \
      char* type = abi::__cxa_demangle(typeid(tp).name(), 0, 0, &status); \
      EXPECT_STREQ(expected, type); \
      free(type); \
  } while (0)

// -------------------------------------
struct Dynamic
{  int  Mem   = 0;                            ///< Member.
   char *Data = new char[3]{'H', 'i', '\0'};  ///< Dynamic memory.

   // -------------------------------------
   Dynamic ()
   {  /*__builtin_puts(__PRETTY_FUNCTION__);*/  }

   Dynamic (const Dynamic &cp)
   : Mem(cp.Mem)
   , Data(new char[3]{'H', 'i', '\0'})
   {  /*__builtin_puts(__PRETTY_FUNCTION__);*/  }

   Dynamic (Dynamic &&mv)
   : Mem(mv.Mem)
   , Data(mv.Data)
   {  mv.Data = nullptr;
      /*__builtin_puts(__PRETTY_FUNCTION__);*/
   }

   ~Dynamic ()
   {  /*__builtin_puts(__PRETTY_FUNCTION__);*/
      delete[] Data;
   }

   // -------------------------------------
   Dynamic &
   operator = (const Dynamic &cp)
   {  /*__builtin_puts(__PRETTY_FUNCTION__);*/
      Mem  = cp.Mem;
      Data = new char[3]{'H', 'i', '\0'};
      return *this;
   }

   Dynamic &
   operator = (Dynamic &&mv)
   {  /*__builtin_puts(__PRETTY_FUNCTION__);*/
      Mem     = mv.Mem;
      Data    = mv.Data;
      mv.Data = nullptr;
      return *this;
   }

   bool
   operator == (const Dynamic &eq) const
   {  return (Mem == eq.Mem) && (!__builtin_strcmp(Data, eq.Data));  }
};
