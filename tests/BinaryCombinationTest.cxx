////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
using testing::Test;
#include "cxx/math.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace        cxx  {
inline namespace math {

enum
{  BIT_1 = (1 << 1),
   BIT_2 = (1 << 2),
   BIT_3 = (1 << 3),
   BIT_4 = (1 << 4)
};

class BinaryCombinationTest : public Test
{  private:

   protected:
      virtual void
      SetUp()
      { }

      virtual void
      TearDown()
      { }

   public:
      BinaryCombinationTest()
      { }

      ~BinaryCombinationTest()
      { }

      //! @todo: Create test unit for `binaryCombination()'
      void generateBinaryCombination() {
          std::vector<u32> arr = binaryCombination<u32>({1, 2, 4, 8});
      }
};

TEST_F(BinaryCombinationTest, generateBinaryCombination) { generateBinaryCombination(); }

} // inline namespace math
} // namespace cxx
