////////////////////////////////////////////////////////////////////////////////
#include <celero/Celero.h>
CELERO_MAIN
using celero::TestFixture;
#include "cxx/lang/String.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, lang)

//--------------------------------------
BASELINE(ConvertUnsigned, std, 100, 10'000)
{ std::string str = std::to_string(123456789u); }

BENCHMARK(ConvertUnsigned, cxx, 100, 10'000)
{ String str = toString(123456789u); }

//--------------------------------------
BASELINE(ConvertSignedPositive, std, 100, 10'000)
{ std::string str = std::to_string(123456789); }

BENCHMARK(ConvertSignedPositive, cxx, 100, 10'000)
{ String str = toString(123456789); }

//--------------------------------------
BASELINE(ConvertSignedNegative, std, 100, 10'000)
{ std::string str = std::to_string(-123456789); }

BENCHMARK(ConvertSignedNegative, cxx, 100, 10'000)
{ String str = toString(-123456789); }

//--------------------------------------
BASELINE(ConvertFloatPositive, std, 100, 10'000)
{ std::string str = std::to_string(1234.56789f); }

BENCHMARK(ConvertFloatPositive, cxx, 100, 10'000)
{ String str = toString(1234.56789f); }

_end_namespace_(cxx, lang)
