////////////////////////////////////////////////////////////////////////////////
#include "cxx/lang/TypeTraits.hpp"
/******************************************************************************/
#include <gtest/gtest.h>
#include "cxx/lang/Defines.hpp"
#include "cxx/lang/Property.hpp"
#include "cxx/lang/String.hpp"
#include "TestHelper.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, lang)

class Tester {
      using ClassName = Tester;
  private:
      int    _m   = 123;
      bool   _b   = true;
      String _str = "123"_s;

  public:
      const std::string
      toString () const {
          std::string str = "123";
          return str;
      }

      _property_(M,   _m,   get,       set);
      _property_(B,   _b,   get,       set);
      _property_(Str, _str, read_only, get);
};

//--------------------------------------
TEST(TypeTraitsTest, is_in)
{   EXPECT_TRUE( (is_in<char*, char*, const char*, bool>::value) );
    EXPECT_TRUE( (is_in<const char*, char*, const char*, char>::value) );

    EXPECT_FALSE( (is_in<int, char*, const char*, long, unsigned>::value) );
}

//--------------------------------------
TEST(TypeTraitsTest, is_char)
{  EXPECT_TRUE(is_char<char>::value);
   EXPECT_TRUE(is_char<char&>::value);

   EXPECT_FALSE(is_char<bool>::value);
   EXPECT_FALSE(is_char<char*>::value);
   EXPECT_FALSE(is_char<char[]>::value);
   EXPECT_FALSE(is_char<int>::value);
   EXPECT_FALSE(is_char<long>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, is_char_convertible)
{  EXPECT_TRUE(is_char_convertible<char>::value);
   EXPECT_TRUE(is_char_convertible<char&>::value);

   EXPECT_FALSE(is_char_convertible<bool>::value);
   EXPECT_FALSE(is_char_convertible<char*>::value);
   EXPECT_FALSE(is_char_convertible<char[]>::value);
   EXPECT_FALSE(is_char_convertible<int>::value);
   EXPECT_FALSE(is_char_convertible<long>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, is_bool)
{  EXPECT_TRUE(is_bool<bool>::value);
   EXPECT_TRUE(is_bool<bool&>::value);

   EXPECT_FALSE(is_bool<char>::value);
   EXPECT_FALSE(is_bool<char&>::value);
   EXPECT_FALSE(is_bool<char*>::value);
   EXPECT_FALSE(is_bool<char[]>::value);
   EXPECT_FALSE(is_bool<int>::value);
   EXPECT_FALSE(is_bool<long>::value);
   EXPECT_FALSE(is_bool<std::string>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, is_bool_convertible)
{  EXPECT_TRUE(is_bool_convertible<bool>::value);
   EXPECT_TRUE(is_bool_convertible<bool&>::value);

   EXPECT_FALSE(is_bool_convertible<char>::value);
   EXPECT_FALSE(is_bool_convertible<char&>::value);
   EXPECT_FALSE(is_bool_convertible<char*>::value);
   EXPECT_FALSE(is_bool_convertible<char[]>::value);
   EXPECT_FALSE(is_bool_convertible<int>::value);
   EXPECT_FALSE(is_bool_convertible<long>::value);
   EXPECT_FALSE(is_bool_convertible<std::string>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, is_numeric)
{  EXPECT_TRUE(is_numeric<int>::value);
   EXPECT_TRUE(is_numeric<long>::value);
   EXPECT_TRUE(is_numeric<float>::value);
   EXPECT_TRUE(is_numeric<double>::value);

   EXPECT_FALSE(is_numeric<void>::value);
   EXPECT_FALSE(is_numeric<bool>::value);
   EXPECT_FALSE(is_numeric<char>::value);
   EXPECT_FALSE(is_numeric<char&>::value);
   EXPECT_FALSE(is_numeric<char*>::value);
   EXPECT_FALSE(is_numeric<char[]>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, is_numeric_convertible)
{  EXPECT_TRUE(is_numeric_convertible<int>::value);
   EXPECT_TRUE(is_numeric_convertible<long>::value);
   EXPECT_TRUE(is_numeric_convertible<float>::value);
   EXPECT_TRUE(is_numeric_convertible<double>::value);

   EXPECT_FALSE(is_numeric_convertible<bool>::value);
   EXPECT_FALSE(is_numeric_convertible<char>::value);
   EXPECT_FALSE(is_numeric_convertible<char&>::value);
   EXPECT_FALSE(is_numeric_convertible<char*>::value);
   EXPECT_FALSE(is_numeric_convertible<char[]>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, to_numeric)
{   Tester t;

    EXPECT_TYPE_EQ("int", typename to_numeric<int>::type);
    EXPECT_TYPE_EQ("long", typename to_numeric<long>::type);
    EXPECT_TYPE_EQ("float", typename to_numeric<float>::type);
    EXPECT_TYPE_EQ("double", typename to_numeric<double>::type);
    EXPECT_TYPE_EQ("int", typename to_numeric<decltype(t.M)>::type);

    EXPECT_TYPE_EQ("void", typename to_numeric<bool>::type);
    EXPECT_TYPE_EQ("void", typename to_numeric<bool*>::type);
    EXPECT_TYPE_EQ("void", typename to_numeric<char>::type);
    EXPECT_TYPE_EQ("void", typename to_numeric<char*>::type);
    EXPECT_TYPE_EQ("void", typename to_numeric<decltype(t.B)>::type);
}

//--------------------------------------
TEST(TypeTraitsTest, is_string_convertible) {
    //Tester t;

    //EXPECT_TRUE(is_string_convertible<bool>::value);
    //EXPECT_TRUE(is_string_convertible<char>::value);
    //EXPECT_TRUE(is_string_convertible<char*>::value);
    //EXPECT_TRUE(is_string_convertible<int>::value);
    //EXPECT_TRUE(is_string_convertible<std::string>::value);
    //EXPECT_TRUE(is_string_convertible<decltype(t.Str)>::value);

    //EXPECT_FALSE(is_string_convertible<decltype(t.M)>::value);
    //EXPECT_FALSE(is_string_convertible<decltype(t.B)>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, is_cstring)
{   Tester t;

    EXPECT_TRUE(is_cstring<char*>::value);
    EXPECT_TRUE(is_cstring<const char*>::value);
    EXPECT_TRUE(is_cstring<char[]>::value);
    EXPECT_TRUE(is_cstring<const char[]>::value);
    EXPECT_TRUE(is_cstring<char (&)[20]>::value);
    EXPECT_TRUE(is_cstring<const char (&)[20]>::value);

    EXPECT_FALSE(is_cstring<bool>::value);
    EXPECT_FALSE(is_cstring<int>::value);
    EXPECT_FALSE(is_cstring<String>::value);
    EXPECT_FALSE(is_cstring<decltype(t.M)>::value);
    EXPECT_FALSE(is_cstring<decltype(t.B)>::value);
    EXPECT_FALSE(is_cstring<decltype(t.Str)>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, is_cstring_convertible)
{   Tester t;

    EXPECT_TRUE(is_cstring_convertible<char*>::value);
    EXPECT_TRUE(is_cstring_convertible<const char*>::value);
    EXPECT_TRUE(is_cstring_convertible<char[]>::value);
    EXPECT_TRUE(is_cstring_convertible<const char[]>::value);
    EXPECT_TRUE(is_cstring_convertible<String>::value);

    EXPECT_FALSE(is_cstring_convertible<bool>::value);
    EXPECT_FALSE(is_cstring_convertible<int>::value);
    EXPECT_FALSE(is_cstring_convertible<decltype(t.M)>::value);
    EXPECT_FALSE(is_cstring_convertible<decltype(t.B)>::value);
    EXPECT_FALSE(is_cstring_convertible<decltype(t.Str)>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, is_fundamental_convertible)
{   Tester t;

    EXPECT_TRUE(is_fundamental_convertible<bool>::value);
    EXPECT_TRUE(is_fundamental_convertible<char>::value);
    EXPECT_TRUE(is_fundamental_convertible<int>::value);
    EXPECT_TRUE(is_fundamental_convertible<decltype(t.M)>::value);
    EXPECT_TRUE(is_fundamental_convertible<decltype(t.B)>::value);

    EXPECT_FALSE(is_fundamental_convertible<bool*>::value);
    EXPECT_FALSE(is_fundamental_convertible<char*>::value);
    EXPECT_FALSE(is_fundamental_convertible<int*>::value);
    EXPECT_FALSE(is_fundamental_convertible<decltype(t.Str)>::value);
}

//--------------------------------------
TEST(TypeTraitsTest, to_fundamental)
{   Tester t;

    EXPECT_TYPE_EQ("bool", typename to_fundamental<bool>::type);
    EXPECT_TYPE_EQ("char", typename to_fundamental<char>::type);
    EXPECT_TYPE_EQ("int",  typename to_fundamental<int>::type);
    EXPECT_TYPE_EQ("bool", typename to_fundamental<decltype(t.B)>::type);
    EXPECT_TYPE_EQ("int",  typename to_fundamental<decltype(t.M)>::type);

    EXPECT_TYPE_EQ("void", typename to_fundamental<bool*>::type);
    EXPECT_TYPE_EQ("void", typename to_fundamental<int*>::type);
    EXPECT_TYPE_EQ("void", typename to_fundamental<char*>::type);
    EXPECT_TYPE_EQ("void", typename to_fundamental<char[]>::type);
    EXPECT_TYPE_EQ("void", typename to_fundamental<String>::type);
    EXPECT_TYPE_EQ("void", typename to_fundamental<decltype(t.Str)>::type);
}

_end_namespace_(cxx, lang)
