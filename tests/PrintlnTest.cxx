////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include "cxx/io/out.hpp"
#include "TestHelper.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, io)
namespace out {

//--------------------------------------
TEST(PrintlnTest, CString)
{  EXPECT_STREAMOUT_EQ("ab AB 12 \n", out::println("ab AB 12"));
   EXPECT_STREAMOUT_EQ("ab AB 12 \n", out::println("ab", "AB", "12"));
}

} // namespace out
_end_namespace_(cxx, io)
