////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
using testing::Test;
#include "cxx/lang/Property.hpp"
#include "cxx/lang/Defines.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx)

class MixType {  };

// =============================================================================
struct Tester
{  using ClassName = Tester;

   int     _i  = 0;
   MixType _mix;
   char    *_str = new char[30];

   ~Tester ()
   {  delete[] _str;  }

   _property_(I,    _i);
   _property_(Mix, _mix, read_only, get);
   _property_(Str, _str, array, set);
};

int  Tester::getI ()                {  return _i;   }
void Tester::setI (const int value) {  _i = value;  }

// =============================================================================
struct StaticTester
{  using ClassName = StaticTester;

   static int     _i;
   static MixType _mix;
   static char    _str[30];

   _property_(I,   _i,   static);
   _property_(Str, _str, static, array, set);
   _property_(Mix, _mix, static);
};

int     StaticTester::_i       = 0;
char    StaticTester::_str[30] = { };
MixType StaticTester::_mix;

/*static*/ int  StaticTester::getI ()                {  return _i;   }
/*static*/ void StaticTester::setI (const int value) {  _i = value;  }

/*static*/ const MixType &
StaticTester::getMix()
{  return _mix;  }

/*static*/ void cxx::StaticTester::setMix (const cxx::MixType&) { }

// =============================================================================
// ::: Testing Class ::: //
class PropertyTest : public Test
{  protected:
      virtual void
      SetUp () { }

      virtual void
      TearDown () { }

   public:
      PropertyTest () { }

      ~PropertyTest () { }

      // -------------------------------------
      void
      operators () { }
};

// ::: Register test units ::: //
TEST_F(PropertyTest, nothing) { }

_end_namespace_(cxx)
