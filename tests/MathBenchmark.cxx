////////////////////////////////////////////////////////////////////////////////
#include <cmath>
#include <celero/Celero.h>
CELERO_MAIN
using celero::TestFixture;
#include "cxx/math.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace        cxx  {
inline namespace math {

/** @brief UtilsBenchmark class. **/
class MathBenchmark : public TestFixture
{   protected:
      unsigned u_value  = 123456789u;
      int      sp_value = 123456789;
      int      sn_value = -123456789;
      float    fp_value = 123.45678f;
      float    fn_value = -123.45678f;
      double   dp_value = 123.45678;
      double   dn_value = -123.45678;
};

//--------------------------------------
BASELINE_F(Abs, std_signed_positive, MathBenchmark, 100, 50'000)
{ std::abs(sp_value); }

//--------------------------------------
BENCHMARK_F(Abs, builtin_signed_positive, MathBenchmark, 100, 50'000)
{ __builtin_abs(sp_value); }

//--------------------------------------
BENCHMARK_F(Abs, cxx_signed_positive, MathBenchmark, 100, 50'000)
{ abs(sp_value); }

}} // namespace cxx::math