////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include "cxx/io/out.hpp"
#include "cxx/lang/String.hpp"
using namespace cxx;
#include "TestHelper.hpp"
////////////////////////////////////////////////////////////////////////////////

class Tester
{     using ClassName = Tester;
   private:
     int         _m   = 123;
     bool        _b   = true;
     std::string _str = "123";

   public:
      const std::string
      toString () const {
         std::string str = "123";
         return str;
      }

     _property_(M,   _m,   get,       set);
     _property_(B,   _b,   get,       set);
     _property_(Str, _str, read_only, get);
};

//--------------------------------------
TEST(PrintTest, CString)
{  EXPECT_STREAMOUT_EQ("ab AB 12", out::print("ab AB 12"));
   EXPECT_STREAMOUT_EQ("ab AB 12", out::print("ab", "AB", "12"));
}

//--------------------------------------
TEST(PrintTest, PercentChar)
{  EXPECT_STREAMOUT_EQ("% % %", out::print("% % %"));
   EXPECT_STREAMOUT_EQ("ab % % % AB", out::print("ab", "% % %", "AB"));
}

//--------------------------------------
TEST(PrintTest, Fundamental)
{  EXPECT_STREAMOUT_EQ("ab AB 12", out::print("ab AB", 12));
   EXPECT_STREAMOUT_EQ("ab AB 123 1.000000", out::print("ab AB", 123, 1.0f));
}

//--------------------------------------
TEST(PrintTest, Boolean)
{  EXPECT_STREAMOUT_EQ("true",  out::print(true));
   EXPECT_STREAMOUT_EQ("false", out::print(false));
}

//--------------------------------------
//TEST(PrintTest, String) {
//   String str = "ab AB korra"_s;
//   EXPECT_STREAMOUT_EQ("ab AB korra", out::print(str));
//}

//--------------------------------------
TEST(PrintTest, StdString) {
    std::string str = "ab AB korra"s;

    EXPECT_STREAMOUT_EQ("ab AB korra", out::print(str));
}

//--------------------------------------
TEST(PrintTest, ImplictTypeCast) {
    Tester t;

    EXPECT_STREAMOUT_EQ("123", out::print(t));
    EXPECT_STREAMOUT_EQ("123", out::print(t.M));
    EXPECT_STREAMOUT_EQ("true", out::print(t.B));
    //EXPECT_STREAMOUT_EQ("123", out::print(t.Str));
}
