////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
using testing::Test;
#include "cxx/lang/String.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, lang)

#define _check_4_(cstr, str, capacity, size) \
   do \
   {  EXPECT_STREQ(cstr, str._cstr); \
      EXPECT_EQ(size, str._size); \
      EXPECT_EQ(capacity, str._capacity); \
   } while ( 0 )

#define _check_3_(cstr, str, capacity) \
   do \
   {  _check_4_(cstr, str, capacity, __builtin_strlen(cstr)); \
   } while ( 0 )

#define _check_2_(cstr, str) \
   do \
   {  _check_3_(cstr, str, (str._size + 1)); \
   } while ( 0 )

#define _check_x_(x, _0, _1, _2, _3, use_this, ...) use_this

#define _check_(cstr, str, ...) \
   _check_x_( , cstr, str, ##__VA_ARGS__, \
             _check_4_(cstr, str, __VA_ARGS__), \
             _check_3_(cstr, str, __VA_ARGS__), \
             _check_2_(cstr, str)  )

class StringTest : public Test
{  protected:
      virtual void
      SetUp ()
      { }

      virtual void
      TearDown ()
      { }

   public:
      StringTest ()
      { }

      ~StringTest ()
      { }

      // ~ ------------------------------------------------------------------
      void
      constructors ()
      {  // - empty - //
         {  String str;
            _check_("", str);
         }

         // - cstr - //
         {  String str("0123456789");
            _check_("0123456789", str);
         }

         // - copy constructor - //
         {  String str_copy("0123456789");
            String str = str_copy;
            _check_("0123456789", str);
         }

         // - move constructor - //
         {  String str(String("0123456789"));
            _check_("0123456789", str);
         }

          // - construct using bool - //
          {   String str = String(true);
              _check_("true", str);
              String str2 = String(false);
              _check_("false", str2);
          }

         // - construct using positive signed number - //
         {  String str = String(10);
            _check_("10", str);
         }

         // - construct using negative signed number - //
         {  String str = String(-10);
            _check_("-10", str);
         }

         // - construct using unsigned number - //
         {  String str = String(10u);
            _check_("10", str);
         }

         // - construct using positive float number - //
         {  String str = String(10.f);
            _check_("10.000000", str);
         }

         // - construct using positive signed number - //
         {  String str = String(-10.f);
            _check_("-10.000000", str);
         }
      }

      void
      shrink ()
      {  String str("aaa");

         str.reserve(512);
         _check_("aaa", str, 513);
         str.shrink();
         _check_("aaa", str);
      }

      void
      assign ()
      {  // ::: char ::: //
         {  String str;
            str.assign('c', 4);
            _check_("cccc", str);
            str.assign('a', 2);
            _check_("aa", str, 5);
         }

         // ::: C-Style string ::: //
         {  String str;
            str.assign("0123", 3);
            _check_("012", str);
            str.assign("abcde", 30);
            _check_("abcde", str);
            str.assign("abcdefghijkl");
            _check_("abcdefghijkl", str);
         }
      }

      void
      insert ()
      {  // ::: insert at 0 ::: //
         {  String str;
            str.insert(0, 12u);
            _check_("12", str);

            str.insert(0, 0u);
            _check_("012", str);
         }

         // ::: signed positive value ::: //
         {  String str("ac");
            str.insert(1, 12);
            _check_("a12c", str);
         }

         // ::: signed negative value ::: ///
         {  String str("ac");
            str.insert(1, -12);
            _check_("a-12c", str);
         }

         // ::: unsigned value ::: //
         {  String str("ac");
            str.insert(1, 12u);
            _check_("a12c", str);
         }

         // ::: floating point positive value ::: //
         {  String str("ac");
            str.insert(1, 12.001f);
            _check_("a12.001000c", str);
         }

         // ::: floating point negative value ::: //
         {  String str("ac");
            str.insert(1, -12.001f);
            _check_("a-12.001000c", str);
         }

         // ::: c style string ::: //
         {  String str("034");
            str.insert(1, "abc");
            _check_("0abc34", str);
         }

         // ::: String type ::: //
         {  String str("034");
            str.insert(1, String("abc"));
            _check_("0abc34", str);
         }

         // ::: char ::: //
         {  String str("034");
            str.insert(1, 'a');
            _check_("0a34", str);
         }
      }

      void
      append ()
      {  // ::: empty string ::: //
         {  String str;
            str.append('a');
            str.append('b');
            str.append('c');
            str.clear();
            str.append('1');
            str.append('2');
            str.append('3');
         }

         // ::: signed positive value ::: //
         {  String str("034");
            str.append(12);
            _check_("03412", str);
         }

         // ::: signed negative value ::: ///
         {  String str("034");
            str.append(-12);
            _check_("034-12", str);
         }

         // ::: unsigned value ::: //
         {  String str("034");
            str.append(12u);
            _check_("03412", str);
         }

         // ::: floating point positive value ::: //
         {  String str("ac");
            str.append(12.000f);
            _check_("ac12.000000", str);
         }

         // ::: floating point negative value ::: //
         {  String str("034");
            str.append(-12.000f);
            _check_("034-12.000000", str);
         }

         // ::: c style string ::: //
         {  String str("034");
            str.append("abc");
            _check_("034abc", str);
         }

         // ::: String type ::: //
         {  String str("034");
            str.append(String("abc"));
            _check_("034abc", str);
         }

         // ::: char ::: //
         {  String str("034");
            str.append('a');
            _check_("034a", str);
            str.append('a');
            str.append('a');
            str.append('a');
            _check_("034aaaa", str);
         }
      }

      void
      operators ()
      {  // ::: operator = ::: //
         {  String str;
            str = "0123";
            _check_("0123", str);
         }

         // ::: operator << ::: //
         {  // ::: signed positive value ::: //
            {  String str("ac");
               str << 12;
               _check_("ac12", str);
            }

            // ::: signed negative value ::: ///
            {  String str("ac");
               str << -12;
               _check_("ac-12", str);
            }

            // ::: unsigned value ::: //
            {  String str("ac");
               str << 12u;
               _check_("ac12", str);
            }

            // ::: floating point positive value ::: //
            {  String str("ac");
               str << 12.000f;
               _check_("ac12.000000", str);
            }

            // ::: floating point negative value ::: //
            {  String str("ac");
               str << -12.000f;
               _check_("ac-12.000000", str);
            }

            // ::: c style string ::: //
            {  String str("12");
               str << "ac";
               _check_("12ac", str);
            }

            // ::: String type ::: //
            {  String str("12");
               str << String("ac");
               _check_("12ac", str);
            }

            // ::: char ::: //
            {  String str("12");
               str << 'a';
               _check_("12a", str);
            }

            // ::: mixture ::: //
            {  String str("Point:");
               str << " x=" << 12 << "; y=" << 12.001f << "; Name=" << "Jack; c=" << 'a';
               _check_("Point: x=12; y=12.001000; Name=Jack; c=a", str);
            }
         }

         // ::: operator += ::: //
         {  String str;
            str += "Hello";
            str += ' ';
            str += "Sir";
            _check_("Hello Sir", str);
         }

         // - operator ""_s - //
         {  String str = "Hello Sir"_s;
            _check_("Hello Sir", str);
         }
      }
};

// ~ Registre test units: ------------------------------------------------------
TEST_F(StringTest, constructors) {  constructors();  }
TEST_F(StringTest, shrink)       {  shrink();        }
TEST_F(StringTest, insert)       {  insert();        }
TEST_F(StringTest, append)       {  append();        }
TEST_F(StringTest, assign)       {  assign();        }
TEST_F(StringTest, operators)    {  operators();     }

_end_namespace_(cxx, lang)
