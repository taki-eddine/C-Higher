////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <cstring>
#include <new>
#include <utility>
#include "cxx/lang/Defines.hpp"
#include "cxx/lang/Types.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, utils)

template<typename Tp>
  struct MemMgr
  {   /**
       * @brief Allocate memory.
       * @param capacity Number of blocks.
       * @return Pointer to the allocated storage.
      **/
      _force_inline_ static Tp *
      mAlloc (const u32 capacity)
      {   Tp *ptr = static_cast<Tp *>(::operator new (sizeof(Tp) * capacity));
          std::memset(ptr, 0, sizeof(Tp) * capacity);
          return ptr;
      }

      /**
       * @brief Deallocate the memory.
       * @param ptr The pointer to the memory.
       * @return void.
      **/
      _force_inline_ static void
      mDealloc (Tp *__restrict__ ptr)
      { ::operator delete (ptr); }

      /**
       * @brief Copy N block.
      **/
      _force_inline_ static void
      mCopy (Tp *__restrict__ dest, const Tp *__restrict__ src, u32 size)
      {   for (; size > 0; --size, ++dest, ++src)
            *dest = *src;
      }

      /**
       * @brief Copy and contruct object.
      **/
      template<typename T = Tp,
               std::enable_if_t<std::is_class<T>::value>...>
        _force_inline_ static void
        mUninitializedCopy (Tp *__restrict__ dest, const Tp *__restrict__ src, u32 size)
        {   for (; size > 0; --size, ++dest, ++src)
              ::new (dest) Tp(*src);
        }

      template<typename T = Tp,
               std::enable_if_t<std::is_fundamental<T>::value>...>
        _force_inline_ static void
        mUninitializedCopy (Tp *__restrict__ dest, const Tp *__restrict__ src, u32 size)
        { mCopy(dest, src, size); }

      /**
       * @brief Allocate and copy data.
       * @param capacity Storage capacity to allocate it.
       * @param cp       Pointer to memory wich to copy from.
       * @param size     Number of elements.
       * @return Pionter to a new allocated memory.
      **/
      _force_inline_ static Tp*
      mAllocAndCopy (const u32 capacity, const Tp *__restrict__ cp, const u32 size)
      {   Tp *tmp = mAlloc(capacity);
          mCopy(tmp, cp, size);
          return tmp;
      }

      _force_inline_ static Tp*
      mAllocAndUninitializedCopy (const u32 capacity, const Tp *__restrict__ cp, const u32 size)
      {   Tp *tmp = mAlloc(capacity);
          mUninitializedCopy(tmp, cp, size);
          return tmp;
      }

      /**
       * @brief Allocate and move data.
       * @param capacity Storage capacity to allocate it.
       * @param mv       Pointer to memory wich to move data from.
       * @param size     Number of elements.
       * @return Pionter to a new allocated memory.
      **/
      template<typename T = Tp,
               std::enable_if_t<std::is_class<T>::value>...>
        _force_inline_ static Tp*
        mAllocAndMove (const u32 capacity, Tp *__restrict__ mv, const u32 size)
        {   Tp *ptr = mAlloc(capacity);

            for (u32 i=0; i < size; ++i)
              *(ptr + i) = std::move(*(mv + i));

            mDealloc(mv);
            return ptr;
        }

      template<typename T = Tp,
               std::enable_if_t<std::is_fundamental<T>::value>...>
        _force_inline_ static Tp*
        mAllocAndMove (const u32 capacity, Tp *__restrict__ mv, const u32 size)
        {   Tp *ptr = mAlloc(capacity);
            mCopy(ptr, mv, size);
            mDealloc(mv);
            return ptr;
        }

      /**
       * @brief Constructs an object in existing memory by passing a parameter pack.
      **/
      template<typename T = Tp, class ... ArgsTp,
               std::enable_if_t<std::is_class<T>::value>...>
        _force_inline_ static void
        mConstruct (Tp *ptr, ArgsTp&& ... args)
        { ::new (ptr) Tp(args...); }

      /**
       * @brief Constructs an object in existing memory by copying.
      **/
      template<typename T = Tp,
               std::enable_if_t<std::is_class<T>::value>...>
        _force_inline_ static void
        mConstruct (Tp *ptr, const Tp &value)
        { ::new (ptr) Tp(value); }

      /**
       * @brief Constructs an object in existing memory by moving.
      **/
      template<typename T = Tp,
               std::enable_if_t<std::is_class<T>::value>...>
        _force_inline_ static void
        mConstruct (Tp *ptr, Tp &&value)
        { ::new (ptr) Tp(std::move(value)); }

      template<typename T = Tp,
               std::enable_if_t<std::is_fundamental<T>::value>...>
        _force_inline_ static void
        mConstruct (Tp *ptr, Tp value)
        { *ptr = value; }

      /**
       * @brief Destroy an object.
      **/
      _force_inline_ static void
      mDestroy (Tp *ptr)
      {   if ( std::is_class<Tp>::value )
            ptr->~Tp();
      }

      /**
       * @brief Destroy a collection of objects.
      **/
      _force_inline_ static void
      mDestroy (Tp *ptr, const u32 size)
      {   if ( std::is_class<Tp>::value )
            for (u32 i=0; i < size; ++i)
              mDestroy(ptr + i);
      }

      /**
       * @brief Destroy and dealloc a collection of objects.
      **/
      _force_inline_ static void
      mDestroyAndDealloc (Tp *ptr, const u32 size)
      {   mDestroy(ptr, size);
          mDealloc(ptr);
      }
  };

_end_namespace_(cxx, utils)
