////////////////////////////////////////////////////////////////////////////////
#pragma once
////////////////////////////////////////////////////////////////////////////////

#define WHITE(str)  "\e[00;37m" str
#define GRAY(str)   "\e[00;100m" str "\e[00;37m"
#define BLUE(str)   "\e[00;34m" str "\e[00;37m"
#define GREEN(str)  "\e[00;32m" str "\e[00;37m"
#define RED(str)    "\e[00;31m" str "\e[00;37m"
#define AQUA(str)   "\e[00;36m" str "\e[00;37m"
#define PURPLE(str) "\e[00;35m" str "\e[00;37m"
#define YELLOW(str) "\e[00;33m" str "\e[00;37m"
