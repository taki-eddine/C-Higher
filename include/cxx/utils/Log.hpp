////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "cxx/io/out.hpp"
#include "cxx/utils/Console.hpp"
////////////////////////////////////////////////////////////////////////////////

#ifdef DEBUG
   #define LOG_BS(format, ...) \
      cxx::io::printf(WHITE("[  VBS  ]  @ %s -%d- : ") format " \n", __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

   #define LOG_INFO(format, ...) \
      cxx::io::printf(PURPLE("[ INFO  ]  @ %s -%d- : ") format " \n", __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

   #define LOG_MSG(format, ...) \
      cxx::io::printf(AQUA("[  MSG  ]  @ %s -%d- : ") format " \n", __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

   #define LOG_WARN(format, ...) \
      cxx::io::printf(YELLOW("[ WARN  ]  @ %s -%d- : ") format " \n", __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

   #define LOG_DEBUG(format, ...) \
      cxx::io::printf(GREEN("[ DEBUG ]  @ %s -%d- : ") format " \n", __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

   #define LOG_ERROR(format, ...) \
      cxx::io::printf(RED("[ ERROR ]  @ %s -%d- : ") format " \n", __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
   #define LOG_BS(format, ...)
   #define LOG_INFO(format, ...)
   #define LOG_MSG(format, ...)
   #define LOG_WARN(format, ...)
   #define LOG_DEBUG(format, ...)
#endif // DEBUG
