////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "cxx/lang/Defines.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, utils)

constexpr u64 RgbToLong (char r, char g, char b);

_end_namespace_(cxx, utils)

// ~ ---------------------------------------------------------------------------
constexpr u64
cxx::utils::RgbToLong (char r, char g, char b)
{  return (r*65536+g*256+b);  }
