////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <type_traits>
#include <string>

#include "cxx/lang/Defines.hpp"
////////////////////////////////////////////////////////////////////////////////

namespace std {
  template<bool value> using bool_constant = std::__bool_constant<value>;
} // namespace std

//----------------------------------------------------------------------------//

_begin_namespace_(cxx, lang)

/**  @brief Sequence **/
template<int ...>
  struct sequence {
  };

/** @brief Generator **/
template<int N, int ... S>
   struct generate_sequence : generate_sequence<N - 1, N - 1, S...> { };

template<int ... S>
   struct generate_sequence<0, S...>
   { using type = sequence<S...>; };

//--------------------------------------
#define _run_test_() \
  (sizeof(yes) == sizeof(test<Tp>(nullptr)))

#define _declval_test_() \
  (sizeof(test( std::declval<Tp>() )) == sizeof(yes))

/** @brief Type traits base struct. **/
struct type_traits
{   using yes = char;
    using no  = long long;
};

//--------------------------------------
template<typename...>
  struct _is_in_helper;

template<typename Tp>
  struct _is_in_helper<Tp>
  { static constexpr bool value = false; };

template<typename Tp, typename Eq1, typename ... Eqn>
  struct _is_in_helper<Tp, Eq1, Eqn...>
  { static constexpr bool value = std::is_same<Tp, Eq1>::value
                                   || _is_in_helper<Tp, Eqn...>::value;
  };

/** @brief Test if type is same one or more types in list. **/
template<typename Tp, typename EqualTp1, typename EqualTp2, typename ... ArgsTp>
  struct is_in
  : public _is_in_helper<Tp, EqualTp1, EqualTp2, ArgsTp...>
  { };

//--------------------------------------
/**
 * @brief This will tesst if a type is a property or not.
 * @tparam Tp type to check it.
**/
template<typename Tp>
  struct is_property : type_traits
  {   template<typename T>
        static yes test (decltype(&T::IsProperty)*);

      template<typename T>
        static no test (...);

      static constexpr bool value = _run_test_();
  };

//--------------------------------------
template<typename>
  struct to_fundamental;

template <typename Tp>
  struct is_fundamental_convertible;

//--------------------------------------
/** @brief is `bool'. **/
template<typename Tp>
  struct is_bool
  : public std::bool_constant< std::is_same< typename std::remove_reference<Tp>::type,
                                             bool >::value
                             >::type
  { };

//--------------------------------------
/** @brief is `bool' convertible. **/
template<typename Tp>
  struct is_bool_convertible
  : public is_bool< typename to_fundamental<Tp>::type >::type
  { };

//--------------------------------------
/** @brief is `char'. **/
template<typename Tp>
  struct is_char
  : public std::bool_constant< std::is_same<std::remove_reference_t<Tp>, char>::value
                             >::type
  { };

//--------------------------------------
/** @brief is `char' convertible. **/
template<typename Tp>
  struct is_char_convertible
  : public is_char< typename to_fundamental<Tp>::type >::type
  { };

//--------------------------------------
/** @brief Check if the type is unsigned integral. **/
template<typename Tp>
  struct is_unsigned_integral
  : public std::bool_constant< std::is_integral<Tp>::value
                                && std::is_unsigned<Tp>::value
                             >::type
  { };

//--------------------------------------
/** @brief Check if the type is signed integral. **/
template<typename Tp>
  struct is_signed_integral
  : public std::bool_constant< std::is_integral<Tp>::value
                                && std::is_signed<Tp>::value
                             >::type
  { };

//--------------------------------------
/** @brief Check if a type is String. **/
template<typename Tp>
  struct is_string {
      static constexpr bool value = std::is_same<Tp, std::string>::value;
  };

//--------------------------------------
/**
 * @brief is string convertible.
 * @tparam Tp Type to work with.
**/
template<class Tp>
  struct is_string_convertible {
      static constexpr bool value = std::is_convertible<Tp, std::string>::value;
  };

// -------------------------------------
/**
 * @brief Check if class has `toString()' method.
 * @tparam Tp Type to check it.
**/
template <class Tp>
   struct has_tostring : type_traits
   {  template<class T>
         static yes test (decltype(&T::toString) *);

      template<class T>
         static no test (...);

      static constexpr bool value = _run_test_();
   };

//--------------------------------------
/**
 * @brief Check if type is cstring.
 * @tparam Tp Type to work with.
**/
template<typename Tp>
  struct is_cstring
  {   using ValueType = typename std::remove_reference<Tp>::type;

      static constexpr bool value = is_in<Tp, char*, const char*>::value
                                     || ( std::is_array<ValueType>::value
                                           && std::is_same< const typename std::remove_extent<ValueType>::type,
                                                            const char
                                                          >::value );
  };

//--------------------------------------
/**
 * @brief Check if type is cstring.
 * @tparam Tp Type to work with.
**/
template<typename Tp>
  struct is_cstring_convertible
  { static constexpr bool value = std::is_convertible<Tp, const char*>::value; };

//--------------------------------------
/** @brief is pure `numeric'. **/
template <typename Tp>
  struct is_numeric
  : public std::bool_constant< std::is_arithmetic<Tp>::value
                                && !is_bool<Tp>::value
                                && !is_char<Tp>::value
                             >::type
  { };

//--------------------------------------
/** @brief is pure `numeric' convertible. **/
template <typename Tp>
  struct is_numeric_convertible
  : public is_numeric< typename to_fundamental<Tp>::type >::type
  { };

//--------------------------------------
/**
 * @brief Convert the type to numeric if possible, or will give void.
 * @tpram Tp type to work with.
**/
template<typename Tp>
  struct to_numeric
  {     using ConvertedType = typename to_fundamental<Tp>::type;
      public:
          using type = typename std::conditional< is_numeric<ConvertedType>::value,
                                                  ConvertedType, void
                                                >::type;
  };

//--------------------------------------
/**
 * @brief Check if a class has an implicit fundamental typecast operator.
 * @tparam Tp Type to wok with.
**/
template <typename Tp>
  struct is_fundamental_convertible
  {   static constexpr bool value =
              !std::is_same<void, typename to_fundamental<Tp>::type>::value;
  };

//--------------------------------------
/**
 * @brief Deduce the implicite typecast operator of class
 * @tparam Tp Type to wok with.
**/
template<typename Tp>
  struct to_fundamental
  {   private:
          static bool               test(const bool);
          static char               test(const char);
          static int                test(const int);
          static unsigned int       test(const unsigned int);
          static long               test(const long);
          static unsigned long      test(const unsigned long);
          static long long          test(const long long);
          static unsigned long long test(const unsigned long long);
          static float              test(const float);
          static double             test(const double);
          static long double        test(const long double);

          static void test(const bool*);
   	      static void test(const char*);
          static void test(const int*);
          static void test(const unsigned int*);
          static void test(const long*);
          static void test(const unsigned long*);
          static void test(const long long*);
          static void test(const unsigned long long*);
          static void test(const float*);
          static void test(const double*);
          static void test(const long double*);

          /** @brief Any pointer type. **/
          template<typename T>
            static void test(const T*);

          static void test(...);

      public:
          using type = decltype(test( std::declval<Tp>() ));
  };

#undef _run_test_
#undef _declval_test_

_end_namespace_(cxx, lang)
