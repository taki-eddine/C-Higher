////////////////////////////////////////////////////////////////////////////////
#pragma once
////////////////////////////////////////////////////////////////////////////////

// Static Constructor ------------------
#define _invoke_static_constructor_1_(class_name) \
__attribute__((constructor, always_inline, optimize(3), visibility("hidden"))) inline void \
StaticConstructor () \
{  class_name::StaticConstructor();  }

#define _invoke_static_constructor_2_(class_name, order) \
__attribute__((constructor(103+( order * 2)), always_inline, optimize(3), visibility("hidden"))) inline void \
StaticConstructor () \
{  class_name::StaticConstructor();  }

#define _invoke_static_constructor_x_(x, arg0, arg1, use_this, ...) use_this
#define _invoke_static_constructor_(...) _invoke_static_constructor_x_(, ##__VA_ARGS__, _invoke_static_constructor_2_(__VA_ARGS__), _invoke_static_constructor_1_(__VA_ARGS__))

// Static Destructor -------------------
#define _invoke_static_destructor_1_(class_name) \
__attribute__((destructor, always_inline, optimize(3))) static inline void \
StaticDestructor () \
{  class_name::StaticDestructor();  }

#define _invoke_static_destructor_2_(class_name, order) \
__attribute__((destructor(103 + ( order * 2 )), always_inline, optimize(3))) static inline void \
StaticDestructor () \
{  class_name::StaticDestructor();  }

#define _invoke_static_destructor_x_(x, arg0, arg1, use_this, ...) use_this

#define _invoke_static_destructor_(...) _invoke_static_destructor_x_(, ##__VA_ARGS__, _invoke_static_destructor_2_(__VA_ARGS__), _invoke_static_destructor_1_(__VA_ARGS__))

// Global object intializer proirity ---
#define _top_init_       __attribute__ ((init_priority(101)))
#define _init_           __attribute__ ((init_priority(102)))
#define _init_order_(p)  __attribute__ ((init_priority(102+(p*2))))
