////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "cxx/lang/Defines.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx)

/**
 * @brief Global object wrapper
 * @tparam Tp Type to work with
**/
template<typename Tp>
   class Global
   {     using  SignatureType = void (*) (Global<Tp> &);
      private:
         Tp             _value         = 0;
         SignatureType  _on_scoupe_out = nullptr;

      public:
         constexpr
         Global (void)
         { }

         constexpr
         Global (Tp value)
         : _value(value)
         { }

         constexpr
         Global (Tp value, SignatureType on_scoupe_out)
         : _value(value)
         , _on_scoupe_out(on_scoupe_out)
         { }

         ~Global (void)
         {  if ( _on_scoupe_out )
               _on_scoupe_out(*this);
         }

         operator Type (void) const
         { return _value; }

         Tp &
         operator = (Tp value)
         { return (_value = value); }
   };

_end_namespace_(cxx)
