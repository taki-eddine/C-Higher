////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <type_traits>
#include <initializer_list>
#include "cxx/lang/Defines.hpp"
#include "cxx/lang/Types.hpp"
////////////////////////////////////////////////////////////////////////////////

// ~ Acess Way -------------------------
#define _static_access_(prop) /* static */
#define _member_access_(prop) _outer_(prop)->

#define _static_storage_ static
#define _member_storage_ /* member */

// ~ Optimize type for class: ----------
#define _ipa_sra_(tp) \
   typename std::conditional \
   <  std::is_class<tp>::value, \
      const tp & /* class use reference */, \
      tp         /* fundamental use value */ \
   >::type

#define _decltype_ipa_sra_(var) \
   _ipa_sra_(decltype(var))

// =============================================================================
// Property
// =============================================================================
#define _property_helper_2_(prop, mem) \
   _default_property_(prop, mem)

#define _property_helper_3_(prop, mem, opt) \
   _##opt##_property_(prop, mem)

#define _property_helper_4_(prop, mem, opt0, opt1) \
   _##opt0##_##opt1##_property_(prop, mem)

#define _property_helper_5_(prop, mem, opt0, opt1, opt2) \
   _##opt0##_##opt1##_##opt2##_property_(prop, mem)

#define _property_helper_6_(prop, mem, opt0, opt1, opt2, opt3) \
   _##opt0##_##opt1##_##opt2##_##opt3##_property_(prop, mem)

// The interim macro that simply strips the excess and ends up with the required macro
#define _property_x_(x, prop, mem, arg0, arg1, arg2, arg3, use_this, ...)  use_this

// The macro that the programmer uses
#define _property_(...) \
   _property_x_( ,##__VA_ARGS__, _property_helper_6_(__VA_ARGS__), \
                                 _property_helper_5_(__VA_ARGS__), \
                                 _property_helper_4_(__VA_ARGS__), \
                                 _property_helper_3_(__VA_ARGS__), \
                                 _property_helper_2_(__VA_ARGS__) )

// =============================================================================
// ~ Get: ------------------------------
#define _default_get_(prop, mem, access_mode)  return _##access_mode##_access_(prop) mem
#define _custom_get_(prop, mem, access_mode)   return _##access_mode##_access_(prop) get##prop()
#define _disabled_get_(prop, mem, access_mode) static_assert(assert, "Property `" #prop "' cannot be accessed, it is write-only.")

// ~ Set: ------------------------------
#define _default_set_(prop, mem, access_mode)  _##access_mode##_access_(prop) mem = value
#define _custom_set_(prop, mem, access_mode)   _##access_mode##_access_(prop) set##prop(value)
#define _disabled_set_(prop, mem, access_mode) static_assert(correct, "Property `" #prop "' cannot be assigned, it is read-only.")

// ~ Use template: --------------------
#define _template_default_
#define _template_custom_
#define _template_disabled_ template<bool correct = false>

// Delete get set function -------------
#define _delete_default_ = delete
#define _delete_custom_
#define _delete_disabled_ = delete

// ~ Build property helper: ------------
#define _property_builder_helper_(prop, mem, get_mode, set_mode, access_mode) \
   struct prop final \
   {  static constexpr bool IsProperty = true; \
      using PropertyType = decltype(mem); \
\
      _template_##get_mode##_ \
         _force_inline_ operator _decltype_ipa_sra_(mem) (void) const \
         {  _##get_mode##_get_(prop, mem, access_mode);  } \
\
      _template_##set_mode##_ \
         _force_inline_ auto & operator = (_decltype_ipa_sra_(mem) value) \
         {  _##set_mode##_set_(prop, mem, access_mode); \
            return *this; \
         } \
   } prop

// ~ Build property: -------------------
#define _property_builder_(prop, mem, get_mode, set_mode, storage_mode) \
   private: \
      _##storage_mode##_storage_ _force_inline_ _decltype_ipa_sra_(mem) get##prop () _delete_##get_mode##_; \
      _##storage_mode##_storage_ _force_inline_ void                    set##prop (_decltype_ipa_sra_(mem) value) _delete_##set_mode##_ ; \
   public: \
      _##storage_mode##_storage_ _property_builder_helper_(prop, mem, get_mode, set_mode, storage_mode)

// =============================================================================
// ~ Member Property: ------------------
#define _default_property_(prop, mem) \
   _property_builder_(prop, mem, custom, custom, member)

#define _get_property_(prop, mem) \
   _property_builder_(prop, mem, default, custom, member)

#define _set_property_(prop, mem) \
   _property_builder_(prop, mem, custom, default, member)

#define _get_set_property_(prop, mem) \
   _property_builder_(prop, mem, default, default, member)

#define _read_only_property_(prop, mem) \
   _property_builder_(prop, mem, custom, disabled, member)

#define _read_only_get_property_(prop, mem) \
   _property_builder_(prop, mem, default, disabled, member)

#define _write_only_property_(prop, mem) \
   _property_builder_(prop, mem, disabled, custom, member)

#define _write_only_set_property_(prop, mem) \
   _property_builder_(prop, mem, disabled, default, member)

// =============================================================================
// ~ Static Property: ------------------
#define _static_property_(prop, mem) \
   _property_builder_(prop, mem, custom, custom, static)

#define _static_get_property_(prop, mem) \
   _property_builder_(prop, mem, default, custom, static)

#define _static_set_property_(prop, mem) \
   _property_builder_(prop, mem, custom, default, static)

#define _static_get_set_property_(prop, mem) \
   _property_builder_(prop, mem, default, default, static)

#define _static_read_only_property_(prop, mem) \
   _property_builder_(prop, mem, custom, disabled, static)

#define _static_read_only_get_property_(prop, mem) \
   _property_builder_(prop, mem, default, disabled, static)

#define _static_write_only_property_(prop, mem) \
   _property_builder_(prop, mem, disabled, custom, static)

#define _static_write_only_set_property_(prop, mem) \
   _property_builder_(prop, mem, disabled, default, static)

// =============================================================================
// ~ Array property: -------------------
#define _array_property_(prop, mem) \
   _array_property_builder_(prop, mem, custom, member)

#define _array_set_property_(prop, mem) \
   _array_property_builder_(prop, mem, default, member)

#define _static_array_property_(prop, mem) \
   _array_property_builder_(prop, mem, custom, static)

#define _static_array_set_property_(prop, mem) \
   _array_property_builder_(prop, mem, default, static)

// ~ Array property Set: ---------------
//! @todo: Change the behavior of default array set, will dealoc and alloc memory for each set.
#define _default_array_set_(prop, mem, access_mode) \
   const _##prop##_t *begin = list.begin(); \
   _##prop##_t       *ptr   = _##access_mode##_access_(prop) mem; \
   for ( ; begin != list.end(); ++begin, ++ptr) \
      *ptr = *begin

#define _custom_array_set_(prop, mem, access_mode) \
   _##access_mode##_access_(prop) set##prop(list)

// ~ Array property builder helper: ----
#define _array_property_builder_helper_(prop, mem, set_mode, access_mode) \
   struct prop final \
   {  _force_inline_ operator const _##prop##_t * (void) const \
      {  return _##access_mode##_access_(prop) mem;  } \
\
      _force_inline_ _##prop##_t & \
      operator [] (const u32 i) const \
      { return _##access_mode##_access_(prop) mem[i]; } \
\
      _force_inline_ auto & \
      operator = (const std::initializer_list<_##prop##_t> list) \
      {  _##set_mode##_array_set_(prop, mem, access_mode); \
         return *this; \
      } \
   } prop

// ~ Array property builder: -----------
#define _array_property_builder_(prop, mem, set_mode, storage_mode) \
   private: \
      using _##prop##_t = typename std::remove_pointer \
                          <  typename std::conditional \
                             <  std::is_array<decltype(mem)>::value, \
                                typename std::remove_extent<decltype(mem)>::type, \
                                decltype(mem) \
                             >::type \
                          >::type; \
\
      _##storage_mode##_storage_ _force_inline_ void set##prop (const std::initializer_list< _##prop##_t > list); \
   public: \
      _##storage_mode##_storage_ _array_property_builder_helper_(prop, mem, set_mode, storage_mode)

// =============================================================================
// Closure: Read only without member
// =============================================================================
#define _closure_(type, closure) \
   private: /** @todo: use '_ipa_sra_' to optimize type **/ \
      type  get##closure (void); \
      void  set##closure (type value); \
   public: \
   struct closure final \
   {  _force_inline_ closure & \
      operator = (type value) \
      {  _outer_(closure)->set##closure(value); \
         return *this; \
      } \
\
      _force_inline_ \
      operator type (void) \
      { return _outer_(closure)->get##closure(); } \
   } closure

// =============================================================================
// Modifier: Write only
// =============================================================================
#define _modifier_(type, modifier) \
   private: /** @todo: use '_ipa_sra_' to optimize type **/ \
      void  set##modifier (type value); \
   public: \
      struct modifier final \
      {  _force_inline_ modifier & \
         operator = (type value) \
         {  _outer_(modifier)->set##modifier(value); \
            return *this; \
         } \
\
         operator type (void) = delete; \
      } modifier
