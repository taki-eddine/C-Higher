////////////////////////////////////////////////////////////////////////////////
#pragma once
//#include <cstdint>
////////////////////////////////////////////////////////////////////////////////
namespace cxx {

// Unsigned ----------------------------
using u8  = __UINT8_TYPE__;  //std::uint8_t;
using u16 = __UINT16_TYPE__; //std::uint16_t;
using u32 = __UINT32_TYPE__; //std::uint32_t;
using u64 = __UINT64_TYPE__; //std::uint64_t;

// Signed ------------------------------
using s8  = __INT8_TYPE__;  //std::int8_t;
using s16 = __INT16_TYPE__; //std::int16_t;
using s32 = __INT32_TYPE__; //std::int32_t;
using s64 = __INT64_TYPE__; //std::int64_t;

} // namespace cxx
