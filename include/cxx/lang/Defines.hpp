////////////////////////////////////////////////////////////////////////////////
#pragma once
////////////////////////////////////////////////////////////////////////////////

#define _length_(in) ( sizeof(in)/sizeof(*in) )
#define _force_inline_ __attribute__((always_inline, optimize(3))) inline
#define _optimize_     __attribute__((optimize(3)))

#define _outer_(field) \
(const_cast<ClassName*>(reinterpret_cast<const ClassName*>(reinterpret_cast<const char *>(this) - __builtin_offsetof(ClassName, field))))

//============================================================================//
#define _begin_namespace_0_(void)

#define _begin_namespace_1_(name) \
   inline namespace name {

#define _begin_namespace_2_(name, ...) \
   inline namespace name { \
   _begin_namespace_1_(__VA_ARGS__)

#define _begin_namespace_3_(name, ...) \
   inline namespace name { \
   _begin_namespace_2_(__VA_ARGS__)

#define _begin_namespace_4_(name, ...) \
   inline namespace name { \
   _begin_namespace_3_(__VA_ARGS__)

//--------------------------------------
#define _begin_namespace_x_(x, arg0, arg1, arg2, use_this, ...) use_this

//--------------------------------------
#define _begin_namespace_(name, ...) \
   namespace name { \
   _begin_namespace_x_\
   ( , ##__VA_ARGS__, \
    _begin_namespace_3_(__VA_ARGS__), \
    _begin_namespace_2_(__VA_ARGS__), \
    _begin_namespace_1_(__VA_ARGS__), \
    _begin_namespace_0_(__VA_ARGS__)  \
   )

//============================================================================//
#define _end_namespace_0_(void)

#define _end_namespace_1_(name) \
   }

#define _end_namespace_2_(name, ...) \
   _end_namespace_1_(__VA_ARGS__) }

#define _end_namespace_3_(name, ...) \
   _end_namespace_2_(__VA_ARGS__) }

#define _end_namespace_4_(name, ...) \
   _end_namespace_3_(__VA_ARGS__) }

//--------------------------------------
#define _end_namespace_x_(x, arg0, arg1, arg2, use_this, ...) use_this

//--------------------------------------
#define _end_namespace_(name, ...) \
   _end_namespace_x_\
   ( , ##__VA_ARGS__, \
     _end_namespace_3_(__VA_ARGS__), \
     _end_namespace_2_(__VA_ARGS__), \
     _end_namespace_1_(__VA_ARGS__), \
     _end_namespace_0_(__VA_ARGS__)  \
   ) }
