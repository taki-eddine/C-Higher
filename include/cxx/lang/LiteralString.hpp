////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "cxx/lang/Defines.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, lang)

/** @brief Compile time string. **/
template<char ... cs>
  struct LiteralString
  { static constexpr char string[sizeof...(cs) + 1] = { cs..., '\0' }; };

template<char ... cs>
  constexpr char LiteralString<cs...>::string[sizeof...(cs) + 1];

_end_namespace_(cxx, lang)
