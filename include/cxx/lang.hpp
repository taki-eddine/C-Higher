////////////////////////////////////////////////////////////////////////////////
#pragma once
////////////////////////////////////////////////////////////////////////////////
/*
 * This header is auto-generated, any modification will be discarded.
**/

#include "cxx/lang/Defines.hpp"
#include "cxx/lang/Global.hpp"
#include "cxx/lang/LiteralString.hpp"
#include "cxx/lang/numeric.hpp"
#include "cxx/lang/Property.hpp"
#include "cxx/lang/StaticInits.hpp"
#include "cxx/lang/String.hpp"
#include "cxx/lang/Types.hpp"
#include "cxx/lang/TypeTraits.hpp"
