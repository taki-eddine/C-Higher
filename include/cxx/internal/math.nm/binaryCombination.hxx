////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <cmath>
#include <initializer_list>
#include <vector>
#include "cxx/lang/Types.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace        cxx  {
inline namespace math {

namespace priv
{  /**
    * @brief Help create combination of binary data.
    * @param in     Input array to create the combination.
    * @param out    Output array wich contains the combinations.
    * @param start  Start combination index from the output array (because we use previous computed values).
    * @param n      Size of the input array.
    * @param index  Start index of the input array to combine with the data.
    * @param insert Start insertation index in the output array.
    * @param level  Recursive depth (Debug only).
    * @return Last insertion index in the output array.
   **/
   template<typename Tp>
      u32 binaryCombinationHelper (const Tp *in, std::vector<Tp>& out, int start, int n, int index, int insert);
} // namespace priv

//--------------------------------------
#define _SFINAE_binaryCombination_unsigned_integral_(type) \
  std::enable_if_t<std::is_unsigned<Tp>::value>...

/**
 * @brief Create Array wich contains all combination posssible of binary data.
 * @param list Binary data.
 * @return Generated Array.
**/
template<typename Tp,
         _SFINAE_binaryCombination_unsigned_integral_(Tp)>
   std::vector<Tp> binaryCombination(std::initializer_list<Tp> list);

//--------------------------------------
/**
 * @brief Show compile time error if the type to work with is not compatible.
 * @return void
**/
template<bool correct = false>
  auto binaryCombination(...);

} // inline namespace math
} // namespace cxx
