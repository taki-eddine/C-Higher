////////////////////////////////////////////////////////////////////////////////
#pragma once
////////////////////////////////////////////////////////////////////////////////
namespace        cxx  {
inline namespace math {

namespace priv {
    //--------------------------------------
    template<typename Tp>
      u32
      binaryCombinationHelper (const Tp *in, std::vector<Tp>& out, int start, int n, int index, int insert)
      {   // - end index is first insert index and minus one to optimize and skip the last block because.
          for (int i = start, end = insert - 1; i < end; ++i)
          {  int next_start = insert;  // - next level `start' is the first insert block.

             // - `j' cycle through the input array to combine with the current data.
             for (int j=index; j < n; ++j, ++insert)
             {  out[insert] = out[i] | in[j];  }

             ++index;  // - increase to the next combination data.
             if ( index < n )
             {  insert = binaryCombinationHelper(in, out, next_start, n, index, insert);  }
          }

          return insert;
      }

} // namespace priv

//--------------------------------------
template<typename Tp,
         _SFINAE_binaryCombination_unsigned_integral_(Tp)>
  std::vector<Tp>
  binaryCombination(std::initializer_list<Tp> list)
  {   std::vector<Tp> out = std::vector<Tp>(std::pow(2, list.size()) - 1);

      // ::: copy the list in the out ::: //
      for (u32 i=0; i < list.size(); ++i)
        out[i] = list.begin()[i];

      priv::binaryCombinationHelper<Tp>(list.begin(), out, 0, list.size(), 1, list.size());

      return out;
  }

//--------------------------------------
template<bool correct>
    auto binaryCombination(...) {
        static_assert(correct, "`binaryCombination' work with unsigned integral type only.");
    }

//==============================================================================
#undef _SFINAE_binaryCombination_unsigned_integral_

} // inline namespace math
} // namespace cxx
