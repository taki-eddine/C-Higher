////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <utility>
#include <string>
using namespace std::literals;

#include "cxx/lang/Defines.hpp"
#include "cxx/lang/TypeTraits.hpp"
#include "cxx/lang/LiteralString.hpp"
#include "cxx/lang/String.hpp"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, io)
namespace out {

namespace priv
{  //--------------------------------------
   template<char ... cs>
     struct LiteralFormatStringBuilder
     {  using format = LiteralString<cs...>;  };

   /** @brief Partial specialization to remove leading space. **/
   template<char ... cs>
     struct LiteralFormatStringBuilder<' ', cs...>
     {  using format = typename LiteralFormatStringBuilder<cs...>::format;  };

   //--------------------------------------
   template<char ... cs>
     auto
     concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>)
      -> LiteralFormatStringBuilder<cs...>;

   template<char ... cs, class ... Ts>
     auto
     concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, bool, Ts ... args)
      -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 's'>(), args...) );

   template<char ... cs, class ... Ts>
     auto
     concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, char, Ts ... args)
      -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'c'>(), args...) );

    template<char ... cs, class ... Ts>
      auto
      concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, int, Ts ... args)
       -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'd'>(), args...) );

   template<char ... cs, class ... Ts>
      auto
      concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, unsigned, Ts ... args)
       -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'u'>(), args...) );

   template<char ... cs, class ... Ts>
      auto
      concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, long, Ts ... args)
       -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'l', 'd'>(), args...) );

   template<char ... cs, class ... Ts>
      auto
      concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, unsigned long, Ts ... args)
       -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'l', 'u'>(), args...) );

   template<char ... cs, class ... Ts>
      auto
      concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, long long, Ts ... args)
       -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'l', 'l', 'd'>(), args...) );

   template<char ... cs, class ... Ts>
      auto
      concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, unsigned long long, Ts ... args)
       -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'l', 'l', 'u'>(), args...) );

   template<char ... cs, class ... Ts>
     auto
     concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, float, Ts ... args)
      -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'f'>(), args...) );

   template<char ... cs, class ... Ts>
     auto
     concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, double, Ts ... args)
      -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'f'>(), args...) );

   template<char ... cs, class ... Ts>
     auto
     concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, double, Ts ... args)
      -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'l', 'f'>(), args...) );

    template<char ... cs, class ... Ts>
      auto
      concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, const char*, Ts ... args)
       -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 's'>(), args...) );

    template<char ... cs, class ... Ts>
      auto
      concatenateLiteralFormatString (LiteralFormatStringBuilder<cs...>, const void*, Ts ... args)
       -> decltype( concatenateLiteralFormatString(LiteralFormatStringBuilder<cs..., ' ', '%', 'p'>(), args...) );

   //--------------------------------------
   template<class ... Ts>
     constexpr const char*
     buildLiteralFormatString ()
     {   using LiteralFormatStringBuilder = decltype( concatenateLiteralFormatString(
                                                        LiteralFormatStringBuilder<>(),
                                                        std::declval<Ts>()...) );

         using Format = typename LiteralFormatStringBuilder::format;

         return Format::string;
     }

    //---------------------------------------
    /** @brief Forward numeric, char and class with fundamental type cast. **/
    template<typename Tp,
             std::enable_if_t< is_numeric_convertible<Tp>::value
                                || is_char_convertible<Tp>::value >... >
      _force_inline_ auto
      forward_arg (const Tp& arg)
       -> typename to_fundamental<Tp>::type
      { return arg; }

    //---------------------------------------
    /** @brief Forward cstring param. **/
    template<typename Tp,
             std::enable_if_t< is_cstring<Tp>::value >... >
      _force_inline_ const char*
      forward_arg (const Tp& arg)
      { return arg; }

    //---------------------------------------
    /** @brief Forward bool argument **/
    template<typename Tp,
             std::enable_if_t< is_bool_convertible<Tp>::value >... >
      _force_inline_ const char*
      forward_arg (const Tp& value)
      { return ("false\0true" + (6 * value)); }

    //---------------------------------------
    /** @brief Forward object with `public String toString()' method. **/
    template<typename Tp,
             std::enable_if_t< has_tostring<Tp>::value >... >
      _force_inline_ const char*
      forward_arg (const Tp& arg) {
          return arg.toString().c_str();
      }

    //---------------------------------------
    /** @brief Forward property of class has `toString()' method. **/
    template<typename Tp,
             std::enable_if_t< is_property<Tp>::value
                                && std::is_class<typename Tp::PropertyType>::value
                                && has_tostring<typename Tp::PropertyType>::value>... >
      _force_inline_ const char*
      forward_arg (const Tp& value) {
          return static_cast<const typename Tp::PropertyType&>(value).toString().c_str();
      }

    /**--------------------------------------
     * @brief Forward object of class `std::string'.
    **/
    template<typename Tp,
             std::enable_if_t< std::is_same<Tp, std::string>::value >... >
    _force_inline_ const char*
    forward_arg(const Tp& value) {
        return value.c_str();
    }

    //---------------------------------------
    /** @brief Forward class we can't handle them and return the address. **/
    template<typename Tp,
             std::enable_if_t< std::is_class<Tp>::value
                                && !is_property<Tp>::value
                                && !has_tostring<Tp>::value
                                && !is_fundamental_convertible<Tp>::value
                                && !std::is_same<Tp, std::string>::value >... >
      _force_inline_ const void*
      forward_arg (const Tp& value)
      { return &value; }

} // namespace priv

//--------------------------------------
template<class ... Ts>
  _force_inline_ void
  print (const Ts& ... args)
  {   // from the right types, create the format string.
      constexpr const char* format_string = priv::buildLiteralFormatString< decltype(priv::forward_arg(args))... >();

      // we have the right types and the right format string call printf.
      // using `unary plus operator +' to call 'operator const char * ()' implicitly for `String' types.
      __builtin_printf(format_string, +priv::forward_arg(args)...);
  }

} // namespace out
_end_namespace_(cxx, io)
