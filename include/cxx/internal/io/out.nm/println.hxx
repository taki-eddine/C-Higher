////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <utility>
#include "cxx/lang/Defines.hpp"
#include "print.hxx"
////////////////////////////////////////////////////////////////////////////////
_begin_namespace_(cxx, io)
namespace out {

//--------------------------------------
template<class ... Ts>
  constexpr void
  println (Ts&& ... args)
  {  print(std::forward<Ts>(args)..., '\n');  }

} // namespace out
_end_namespace_(cxx, io)
