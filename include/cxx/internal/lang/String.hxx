////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <limits>
#include "cxx/lang/TypeTraits.hpp"
#include "cxx/lang/Property.hpp"
#include "cxx/utils/Memory.hpp"
#include "cxx/lang/numeric/NumberTraits.hpp"
////////////////////////////////////////////////////////////////////////////////

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
//                              -* Declaration *-                             //
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
namespace        cxx  {
inline namespace lang {

/** @brief String class. **/
class [[ deprecated ("use `cxx::string` instead.") ]] String : private MemMgr<char>
{   using ClassName = String;
    friend class StringTest;
    friend class StringBenchmark;

  private:
    u32  _size     = 0;
    u32  _capacity = 0;
    char *_cstr    = nullptr;

    //--------------------------------------
    /**
     * @brief Make a gap inside the string.
     * @param offset The offset.
     * @param count  The Gap size.
     * @return void
    **/
    void _makeGap(u32 offset, u32 count);

    //--------------------------------------
    #define _SFINAE_convert_signed_integral_(Tp) \
      std::enable_if_t<std::is_integral<Tp>::value && std::is_signed<Tp>::value>...

    /**
     * @brief Convert signed data and insert it inside the string.
     * @tparam Tp Type must be signed.
     * @param num    A signed data.
     * @param offset The offset.
    **/
    template<typename Tp, int Width = 0,
             _SFINAE_convert_signed_integral_(Tp)>
      int
      _convert(u32 offset, Tp value);

    //--------------------------------------
    #define _SFINAE_convert_unsigned_integral_(Tp) \
      std::enable_if_t<std::is_integral<Tp>::value && std::is_unsigned<Tp>::value>...

    /**
     * @brief Convert unsigned data and insert it inside the string.
     * @tparam Tp Type must be signed.
     * @param num    A signed data.
     * @param offset The offset.
    **/
    template<typename Tp, int Width = 0,
             _SFINAE_convert_unsigned_integral_(Tp)>
      int
      _convert(u32 offset, Tp value);

    //--------------------------------------
    #define _SFINAE_convert_floating_point_(Tp) \
      std::enable_if_t<std::is_floating_point<Tp>::value>...

    /**
     * @brief Convert floating point data and insert it inside the string.
     * @tparam Tp Type must be signed.
     * @param num    A signed data.
     * @param offset The offset.
    **/
    template<class Tp, int Precision = std::numeric_limits<Tp>::digits10,
             _SFINAE_convert_floating_point_(Tp)>
      int
      _convert(u32 offset, Tp value);


    //---------------------------------------
    /**
     * @brief Unsigned To Array.
     * @param value Value to convert it.
     * @param index The number of char need to represent the value.
     * @param str Insert in this object.
    **/
    template<typename Tp, std::enable_if_t<is_unsigned_integral<Tp>::value>...>
      static _force_inline_ void
      _utoa(Tp value, std::size_t index, String& str)
      {   do
          {  str[--index] = "0123456789"[value % 10];
          } while( (value /= 10) );
      }

  public:
    /** @brief Default contructor **/
    String();

    /** @brief Copy contructor **/
    String(const String &str);

    /** @brief Move contructor **/
    String(String &&str);

    /** @brief Contruct from a cstring **/
    String(const char *str);

    /** @brief Contruct from a cstring with n char **/
    String(const char *cstr, std::size_t len);

    String(const bool value);

    //--------------------------------------
    #define _SFINAE_String_arithmetic_(type) \
      std::enable_if_t<std::is_arithmetic<type>::value>...

    /**
     * @brief Consturct witch insert a numeric value.
     * @param value Value to initiate the string.
    **/
    template<typename Tp,
             _SFINAE_String_arithmetic_(Tp)>
      String(Tp value);

    /** @brief Destructor **/
    ~String();

    //--------------------------------------
    //! @todo: Add "bool operator == (const String&)".
    //! @todo: Add "String operator + (const String&)".
    //! @todo: Add "const String& operator = (String &&str)".
    operator const char *() const;
    String& operator =(const char *cstr);
    String& operator +=(const String &str);
    String& operator +=(const char c);
    String& operator +=(const char *cstr);

    ///
    _force_inline_ char&
    operator[](const u32 pos);

    ///
    template<class T>
       String& operator << (T arg);

    //--------------------------------------
    //! @todo: Add static function to handle c style string.
    static inline void copy(char *__restrict dest, const char *__restrict src);
    static inline bool equals(const char *__restrict cstr1, const char *__restrict cstr2);

    //---------------------------------------
    #define _SFINAE_valueOf_integral_(Tp) \
      std::enable_if_t<std::is_integral<Tp>::value>...

    ///
    template<typename Tp,
             _SFINAE_valueOf_integral_(Tp)>
      static String
      valueOf(Tp value);

    //---------------------------------------
    #define _SFINAE_valueOf_floating_point_(Tp) \
      std::enable_if_t<std::is_floating_point<Tp>::value>...

    ///
    template<typename Tp,
             _SFINAE_valueOf_floating_point_(Tp)>
      static String
      valueOf(Tp value);

    //--------------------------------------
    inline void reserve(u32 new_cap);

    /**
     * @brief Replaces the contents with count copies.
     * @param c     The character.
     * @param count The count copies.
     * @return void
    **/
    void assign(char c, u32 count);

    /** @brief Replaces **/
    void assign(const char *cstr, u32 n);

    /**
     * @brief Replace the content.
     * @param cstr
    **/
    void assign(const char *cstr);

    void assign(const String &str, u32 n);
    void assign(String &&str);

    template<class Tp>
       _force_inline_ void append(Tp data);

    template<typename Tp,
             std::enable_if_t<std::is_arithmetic<Tp>::value>...>
      int
      insert(u32 offset, Tp value);

    /**
     * @brief Insert a string.
     * @param offset The offset.
     * @param str    The string.
    **/
    int insert(u32 offset, const String &str);

    /**
     * @brief Insert C style string.
     * @param offset The offset.
     * @param cstr   The string.
    **/
    int insert(u32 offset, const char *cstr);

    /**
     * @brief Insert character.
     * @param c      The char.
     * @param offset The offset.
    **/
    int insert(u32 offset, const char c);

    bool equals(const String &str);
    bool equals(const char *cstr);

    void shrink(void);
    inline void clear(void);

    /**
     * @brief Return a C Style string.
     * @return C Style String.
    **/
    const char* cStr() const;

    /**
     * @brief Return the same string.
     * @return Reference to this string.
    **/
    const String& toString() const;

    //--------------------------------------
    _property_(Size,      _size,     read_only, get);
    _property_(Capacity,  _capacity, read_only, get);
};

//============================================================================//
//--------------------------------------
/** @brief Convert value to String. **/
template<typename Tp>
  _force_inline_ String
  toString(Tp value);

}} // namespace cxx::lang

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
//                            -* Implementation *-                            //
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
namespace        cxx  {
inline namespace lang {

//--------------------------------------
template<typename Tp,
         _SFINAE_valueOf_integral_(Tp)>
  /*static*/ String
  String::valueOf(Tp value)
  {   String      str         = String();
      const bool  is_negative = std::is_signed<Tp>::value && (value < 0);

      str._size = utils::countDigits(value) + is_negative;
      str.reserve(str._size);

      if( std::is_signed<Tp>::value )
        str[0] = '-';

      _utoa<std::make_unsigned_t<Tp>>(abs(value), str._size, str);
      return str;
  }

//--------------------------------------
template<typename Tp,
         _SFINAE_valueOf_floating_point_(Tp)>
  /*static*/ String
  String::valueOf(Tp value)
  {   String str        = String();
      bool  is_negative = value < 0.;
      long integral_part = static_cast<long>(value);
      long fraction_part = abs( (value - integral_part) * __builtin_pow(10, std::numeric_limits<Tp>::digits10) )
                            + std::numeric_limits<Tp>::round_error();

      std::size_t ndigit_integral = utils::countDigits(integral_part);
      std::size_t ndigit_fraction = std::numeric_limits<Tp>::digits10;

      str._size = 1 + ndigit_integral + ndigit_fraction + is_negative;
      str.reserve(str._size);
      str[0] = '-';

      std::size_t index = str._size;

      // fraction
      do
      {  str[--index] = "0123456789"[fraction_part % 10];
      } while( (value /= 10) );

      for(std::size_t end = str._size - ndigit_fraction; index > end; --index)
        str[index] = '0';

      // dot
      str[index] = '.';

      // integral
      _utoa<std::make_unsigned_t<long>>(integral_part, ndigit_integral, str);

      return str;
  }

}} // namespace cxx::lang
