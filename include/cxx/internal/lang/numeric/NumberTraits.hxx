////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <type_traits>
#include "cxx/lang/Defines.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace        cxx   {
inline namespace utils {

//--------------------------------------
#define _SFINAE_countDigits_signed_integral_(type) \
  std::enable_if_t<std::is_integral<type>::value && std::is_signed<type>::value>...

/**
 * @brief Counts the number of digits of signed types.
 * @tparam Tp Type to work with.
 * @param num The integral arguments.
 * @return The number of digits.
**/
template<typename Tp,
         _SFINAE_countDigits_signed_integral_(Tp)>
  _force_inline_ constexpr int
  countDigits(Tp value);

//--------------------------------------
#define _SFINAE_countDigits_unsigned_integral_(type) \
  std::enable_if_t<std::is_integral<type>::value && std::is_unsigned<type>::value>...

/**
 * @brief Counts the number of digits of unsigned types.
 * @tparam Tp Type to work with.
 * @param num The integral arguments.
 * @return The number of digits.
**/
template<typename Tp,
         _SFINAE_countDigits_unsigned_integral_(Tp)>
  constexpr int
  countDigits(Tp value);

/** @brief Generates a compile time error if the type is not a integral. **/
template<bool correct = false>
  constexpr auto
  countDigits(...);

}} // namespace cxx::utils
