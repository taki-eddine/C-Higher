////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <cmath>
////////////////////////////////////////////////////////////////////////////////
namespace        cxx   {
inline namespace utils {
//! @todo: add template intentiation for these functions.

//-------------------------------------
template<typename Tp,
         _SFINAE_countDigits_signed_integral_(Tp)>
  _force_inline_ constexpr int
  countDigits(Tp value)
  { return countDigits<std::make_unsigned_t<Tp>>(std::abs(value)); }

//--------------------------------------
template<typename Tp,
         _SFINAE_countDigits_unsigned_integral_(Tp)>
  constexpr int
  countDigits(Tp value)
  { return (value < 10 ? 1 :
            (value < 100 ? 2 :
             (value < 1000 ? 3 :
              (value < 10000 ? 4 :
               (value < 100000 ? 5 :
                (value < 1000000 ? 6 :
                 (value < 10000000 ? 7 :
                  (value < 100000000 ? 8 :
                   (value < 1000000000 ? 9 : 10)))))))));
  }

//--------------------------------------
template<bool correct = false>
  constexpr auto
  countDigits(...)
  { static_assert(correct, "'countDigits()' Accepts only integral type"); }

//==============================================================================
#undef _SFINAE_countDigits_signed_integral_
#undef _SFINAE_countDigits_unsigned_integral_

}} // namespace cxx::utils
