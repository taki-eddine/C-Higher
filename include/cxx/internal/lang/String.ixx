////////////////////////////////////////////////////////////////////////////////
#pragma once
////////////////////////////////////////////////////////////////////////////////
namespace        cxx  {
inline namespace lang {

// === [Public] ============================================================= //
// -------------------------------------
/*static*/ _force_inline_ void
String::copy (char *__restrict dest, const char *__restrict src)
{  for ( ; (*dest = *src); ++dest, ++src )
   { }
}

// -------------------------------------
/*static*/ _force_inline_ bool
String::equals (const char *__restrict cstr1, const char *__restrict cstr2)
{  for ( ; *cstr2; ++cstr1, ++cstr2)
      if ( *cstr1 != *cstr2 )
         return false;
   return true;
}

//--------------------------------------
_force_inline_ char&
String::operator[](const u32 pos)
{ return _cstr[pos]; }

// -------------------------------------
_force_inline_ void
String::reserve (u32 new_capacity)
{  new_capacity += 1;  // add 1 for the delimiter char '\0'.

   if ( new_capacity > _capacity )
   {  _capacity = new_capacity;
      _cstr = this->mAllocAndMove(_capacity, _cstr, _size);
   }
}

// -------------------------------------
_force_inline_ void
String::clear (void)
{ _cstr[(_size = 0)] = '\0'; }

}} // namespace cxx::lang

// ========================================================================== //
_force_inline_ cxx::String
operator""_s(const char *cstr, std::size_t len)
{ return cxx::String(cstr, len); }

//! @todo: Add numeric literals.
