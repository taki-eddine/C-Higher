////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <cmath>
#include <algorithm>
#include "cxx/lang/numeric/NumberTraits.hpp"
#include "cxx/math.hpp"
////////////////////////////////////////////////////////////////////////////////
namespace        cxx  {
inline namespace lang {

// === [Private] ============================================================ //
// -------------------------------------
template<typename Tp, int Width,
         _SFINAE_convert_signed_integral_(Tp)>
  int
  String::_convert(u32 offset, Tp value)
  {   int is_neg = (value < 0);

      //! @todo: we must allocate the needed space for the number before inserting '-'.
      if ( is_neg )
      {   insert(offset, '-');
          offset += 1;
      }

      return is_neg + _convert<std::make_unsigned_t<Tp>, Width>
                      (offset, std::abs(value));
  }

// -------------------------------------
template<typename Tp, int Width,
         _SFINAE_convert_unsigned_integral_(Tp)>
  int
  String::_convert(u32 offset, Tp value)
  {   constexpr char digit_char[] = { '0', '1', '2', '3', '4', '5',
                                      '6', '7', '8', '9'
                                    };

      u32 count_digits = std::max(utils::countDigits(value), Width);
      u32 i            = offset + count_digits;

      _makeGap(offset, count_digits);

      // - convert and insert - //
      do
      {  _cstr[--i] = digit_char[(value % 10)];  //! @todo: use array and acced directly to char without calculate it.
      } while( (value /= 10) );

      // - fill extra chars with zero - //
      while( i > offset )
        _cstr[--i] = '0';

      _size += count_digits;
      return count_digits;
  }

// -------------------------------------
template<typename Tp, int Precision,
         _SFINAE_convert_floating_point_(Tp)>
  int
  String::_convert(u32 offset, Tp value)
  {   int  nchar = 0;
      long integral_part = (long)value;
      long fraction_part = std::abs( (value - integral_part)
                                       * __builtin_pow(10, Precision) )
                            + std::numeric_limits<Tp>::round_error();

      nchar  = _convert(offset, integral_part);
      nchar += insert(offset + nchar, '.');
      nchar += _convert<long, Precision>(offset + nchar, fraction_part);
      return nchar;
  }

// === [Public] ============================================================= //
// -------------------------------------
template<typename Tp,
         _SFINAE_String_arithmetic_(Tp)>
  String::String(Tp value)
  { _convert(0, value); }

// -------------------------------------
template<class T>
   String&
   String::operator << (T arg)
   {  append(arg);
      return *this;
   }

// -------------------------------------
template<typename Tp,
         std::enable_if_t<std::is_arithmetic<Tp>::value>...>
  int
  String::insert (u32 offset, Tp value)
  { return _convert(offset, value); }

// -------------------------------------
template<class Tp>
   _force_inline_ void String::append (Tp data)
   { insert(_size, data); }

//==============================================================================
//--------------------------------------
template<typename Tp>
  _force_inline_ String
  toString(Tp value)
  { return String::valueOf(value); }

//--------------------------------------
#undef _SFINAE_convert_signed_integral_
#undef _SFINAE_convert_unsigned_integral_
#undef _SFINAE_convert_floating_point_
#undef _SFINAE_String_arithmetic_

}} // namespace cxx::lang
